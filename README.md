### A/B Client - Java
This README documents steps that are necessary to use the java package in a Java project.

#### Working
Given a compressed experiment data from headers, and an experiment name, it returns the result of that experiment.

### Installation (Build Executable JAR)
1. Pull the repository locally
2. Go to the project directory `**/abclient-java`
3. Run the command `mvn clean package` in the terminal
4. A `abclient-java-1.0-SNAPSHOT-FINAL.jar` file will be generated inside the target folder.
5. Add this jar file to the Java project

#### Usage
```
import com.pharmeasy.ABClient;

ABClient.GetResult(<compressed_data>, <experiment_name>);
```

#### Example
```
public static void main(String[] args) {

    String src = "9RF7CiAgICAiZXhwZXJpbWVudHMiOiBbCiAgICAgICAgewoA+hMgICAgIm5hbWUiOiAiVGVzdGluZyBmb3IgU3Vicy1udiIsKwB8aWQiOiAiQRcAi3Jlc3VsdCI6WgD7DiAgICAiaXNfZ2VuZXJpY192YWx1ZSI6ICIwLjMwWwD4AiAgICAibWFyZ2luX2Jvb3N0LgAfMi4AAs5zYWxlc190cmVuZHM0AEI1MCIKVwACAgAVfbMAFn2+AAqwAAUKAfYNYXV0b21hdGlvbl8xNDUwMDYzMTI2NDU5MjAyIj0AAt8Aa2QiOiAiQs4ADxIBCv8HQVVUT01BVElPTl9URVNUXzEiOiAiNeMAAgwrAG8yIjogIjMrABJvMyI6ICIyBQEBAA8BsCB9CiAgICAgXQp9";

    String experiment_name = "Testing for Subs-nv";

    String result = ABClient.GetResult(src, experiment_name);
    
    System.out.println(result);
}
```
##### Output
```
{"is_generic_value":"0.30","sales_trends_boost_value":"0.50","margin_boost_value":"0.20"}
```

##### The above src is an LZ4 compressed string of the following json:
```
{
    "experiments": [
        {
            "name": "Testing for Subs-nv",
            "id": "A",
            "result": {
                "is_generic_value": "0.30",
                "margin_boost_value": "0.20",
                "sales_trends_boost_value": "0.50"
            }
        },
        {
            "name": "automation_1450063126459202",
            "id": "B",
            "result": {
                "AUTOMATION_TEST_1": "50",
                "AUTOMATION_TEST_2": "30",
                "AUTOMATION_TEST_3": "20"
            }
        }
    ]
}
```   
##### To Run Test
```
mvn test
```   







 