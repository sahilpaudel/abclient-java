package com.pharmeasy;

import net.jpountz.lz4.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Base64;

public class ABClient {

    public static String GetResult(String data, String experimentName) {
        String decompressed = decompress(data);
        JSONObject object = new JSONObject(decompressed);

        try {
            JSONArray experiments = object.getJSONArray("experiments");
            for (int i = 0; i < experiments.length(); i++) {
                JSONObject experiment = experiments.getJSONObject(i);
                String exptName = experiment.getString("name");

                if (exptName.equals(experimentName)) {
                    return experiment.get("result").toString();
                }
            }
        } catch (JSONException e) {
            if (e.getMessage().contains("not found")) {
                System.out.println("Please check if the compressed data " + e.getMessage());
                System.out.println("UnCompressed Data : " + decompressed);
                return null;
            }
        }

        return null;
    }

    private static String decompress(String data) {
        LZ4Factory safeFactory = LZ4Factory.safeInstance();
        LZ4SafeDecompressor decompressor = safeFactory.safeDecompressor();

        byte[] decoded = Base64.getDecoder().decode(data);
        byte[] decomp = new byte[decoded.length * 5];
        decompressor.decompress(decoded, decomp);
        return new String(decomp);
    }
}
