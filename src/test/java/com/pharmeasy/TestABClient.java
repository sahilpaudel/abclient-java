package com.pharmeasy;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TestABClient {

    @Test
    public void shouldReturnExperimentResult() {
        String encodedString = "8Qd7CiAgICAiZXhwZXJpbWVudHMiOiBbFQAAAgACHwAEAgD1DyJuYW1lIjogIlRlc3RpbmcgZm9yIFN1YnMtbnYiLDUAAAIAMCJpZCkAHEEXAItyZXN1bHQiOloAAAIA8QEiaXNfZ2VuZXJpY192YWx1agBLMC4zMEQAAAIA2CJtYXJnaW5fYm9vc3QuAB8yLgACznNhbGVzX3RyZW5kczQAOTUwIrwAFX0OABZ91QAKsAAFCgH7DGF1dG9tYXRpb25fMTQ1MDA2MzEyNjQ1OTIwMrcAAxIBHEIXAA8SAQnwAkFVVE9NQVRJT05fVEVTVF8xVAEfNeMAAgwrABAyKwAPPAEDDCsAEDMrAB8yBQEHkAogICAgIF0KfQ==";
        ABClient abClient = new ABClient();
        String result = abClient.GetResult(encodedString, "automation_1450063126459202");
        String expectedString = "{\"AUTOMATION_TEST_1\":\"50\",\"AUTOMATION_TEST_2\":\"30\",\"AUTOMATION_TEST_3\":\"20\"}";

        assertEquals(expectedString, result);
    }

    @Test
    public void shouldReturnNull() {
        String encodedString = "8Qd7CiAgICAiZXhwZXJpbWVudHMiOiBbFQAAAgACHwAEAgD1DyJuYW1lIjogIlRlc3RpbmcgZm9yIFN1YnMtbnYiLDUAAAIAMCJpZCkAHEEXAItyZXN1bHQiOloAAAIA8QEiaXNfZ2VuZXJpY192YWx1agBLMC4zMEQAAAIA2CJtYXJnaW5fYm9vc3QuAB8yLgACznNhbGVzX3RyZW5kczQAOTUwIrwAFX0OABZ91QAKsAAFCgH7DGF1dG9tYXRpb25fMTQ1MDA2MzEyNjQ1OTIwMrcAAxIBHEIXAA8SAQnwAkFVVE9NQVRJT05fVEVTVF8xVAEfNeMAAgwrABAyKwAPPAEDDCsAEDMrAB8yBQEHkAogICAgIF0KfQ==";
        ABClient abClient = new ABClient();
        String result = abClient.GetResult(encodedString, "experimentNotPresent");
        assertNull(result);
    }

    @Test
    public void shouldReturnNullForEmptyExperiment() {
        String encodedString = "8Qd7CiAgICAiZXhwZXJpbWVudHMiOiBbFQAAAgACHwAEAgD1DyJuYW1lIjogIlRlc3RpbmcgZm9yIFN1YnMtbnYiLDUAAAIAMCJpZCkAHEEXAItyZXN1bHQiOloAAAIA8QEiaXNfZ2VuZXJpY192YWx1agBLMC4zMEQAAAIA2CJtYXJnaW5fYm9vc3QuAB8yLgACznNhbGVzX3RyZW5kczQAOTUwIrwAFX0OABZ91QAKsAAFCgH7DGF1dG9tYXRpb25fMTQ1MDA2MzEyNjQ1OTIwMrcAAxIBHEIXAA8SAQnwAkFVVE9NQVRJT05fVEVTVF8xVAEfNeMAAgwrABAyKwAPPAEDDCsAEDMrAB8yBQEHkAogICAgIF0KfQ==";
        ABClient abClient = new ABClient();
        String result = abClient.GetResult(encodedString, "");
        assertNull(result);
    }

    @Test
    public void shouldReturnNullForExperimentKeyNotPresent() {
        String encodedString = "8xV7CiAgICJzdGF0dXMiOjEsCiAgICJkYXRhIjpbCiAgICAgIHsIAPcZICAgIm5hbWUiOiJhdXRvbWF0aW9uXzExMDQ0NzIyMTEzNzI2OTgiLC8A+RBkZXNjcmlwdGlvbiI6ImNyZWF0ZSBleHBlcmltZW50LAD5DGdvYWwiOiJUZXN0IHdpdGggdmFsaWQgZGF0YSgARnRhZ3OdAAICAKgiaG9tZV9wYWdlfwCSICAgImxvZ2luGgAGRAAYXVAArWlkZW50aWZpZXJXAAEdAQgCAMcic2VnbWVudCI6OTVEAAICAP4BInBsYXRmb3JtIjoiaW9zIiEA/gRkZXZpY2VfdmVyc2lvbiI6NS42JQBiZXhwcmVzIQAEcABZPT05NSIqABZ9DgAJzgBKbWV0Yb8BAgIA/AMiQVVUT01BVElPTl9URVNUXzEqAQwhAB8yIQANFzOSAAmEAAXgAA9OAQ3JQ3JlYXRlZEJ5Ijp7KwACAgAiIk5uAmx0ZXN0IHDbAQICAPsNIkVtYWlsIjoidGVzdHBlQHBoYXJtZWFzeS5pbsEAAgIA9xYiRXZlbnRUaW1lIjoiMjAyMC0wNS0yMVQwMjo1MjoyMy44NTZa2gACAgAdfUsAvyJTd2l0Y2hlZE9uwAB8LzkxwAASn1R1cm5lZE9mZr8ADQ9HAQQDeAEPHgAEB2UB/wQwMDAxLTAxLTAxVDAwOjAwOjAwYQESb0NvbXBsZSACEA+DAAQPoQBOb0FyY2hpdsACDw+gAGXdZXhwb3N1cmUiOjEwMIQAjyJzdGFydF90CgMGHlq4AP8HInNjaGVkdWxlcl9jaGVjayI6dHJ1ZVsAAo90dXMiOiJvbkUAAP8Ad2lubmluZ192YXJpYW50IgECB5cFDEgEAgIADZwFAgIABKIGCDkFDYIAAgIAfyJzcGxpdF8uBQEIAgAPaQAEAgIAfyJpZCI6IkFjAAUCAgACaQBPIjo0MCcAC89yZXR1cm5fdmFsdWVSBAMIAgAP0gUATjoiNTBwAQsCAA04AF8yIjoiMzgAIG0zIjoiMjDKAwgCAAemBgsCAA5RBQUCAA9xARAfQq4ACwRxAR429gIIAgAPcQExDzkBIV8yIjoiM5cCBgUCAA2pARIzOAAPHwEJD3EBCA8aAAMfXRcAAA1FAAolAAcfAAAZANAgICAgfQogICBdCn0K";
        ABClient abClient = new ABClient();
        String result = abClient.GetResult(encodedString, "");
        assertNull(result);
    }
}
